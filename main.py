import sys
import pygame
from neuropongoid.game.game import Game
from neuropongoid.game.control.controllistener import ControlListener
from neuropongoid.game.control.humancontrol import HumanControl
from neuropongoid.game.control.fuzzycontrol import FuzzyControl
from neuropongoid.game.control.dummycontrol import DummyControl
import neuropongoid.game.control.settings as control_settings


def main():
    # Two human players
    # p1_control = HumanControl(control_settings.KEYBOARD_CONTROL_1_SETTINGS)
    # p2_control = HumanControl(control_settings.KEYBOARD_CONTROL_2_SETTINGS)

    # Player vs fuzzy controller
    #p1_control = HumanControl(control_settings.KEYBOARD_CONTROL_1_SETTINGS)
    #p2_control = FuzzyControl()

    # Two fuzzy controllers
    #p1_control = FuzzyControl()
    #p2_control = FuzzyControl()

    # Fuzzy vs dummy
    p1_control = DummyControl()
    p2_control = FuzzyControl()

    # Two dummies
    #p1_control = DummyControl()
    #p2_control = DummyControl()

    control_listener = ControlListener([p1_control, p2_control])

    pygame.init()
    game = Game(p1_control, p2_control)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        keys_pressed = pygame.key.get_pressed()
        control_listener.trigger(keys_pressed, game.state)
        game.loop()


if __name__ == '__main__':
    main()

