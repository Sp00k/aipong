from enum import Enum


class State(Enum):
    PADDLE1_CENTER_X = 0,
    PADDLE1_Y = 1
    PADDLE2_Y = 2
    PADDLE2_CENTER_X = 3,
    BALL_X = 4,
    BALL_Y = 5
