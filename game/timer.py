class Timer:
    def __init__(self, ticks: int):
        self._initial_count = ticks
        self.count = ticks

    def tick(self):
        if self.count > 0:
            self.count -= 1
        return self.count == 0

    def reset(self):
        self.count = self._initial_count
