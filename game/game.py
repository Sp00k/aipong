from random import randrange
from neuropongoid.game.player import Player
from neuropongoid.game.screen.screen import Screen
from neuropongoid.game.gameobjectmanager import GameObjectManager
from neuropongoid.game.control.paddlecontrol import PaddleControl
from neuropongoid.game.objects.ball import Ball
from neuropongoid.game.screen.painter import Painter
from neuropongoid.game.timer import Timer
from neuropongoid.ai.environment import State
from neuropongoid.game.control.fuzzycontrol import FuzzyControl


class Game:
    MIN_CONTROL_REVERSAL_TICK = 10000
    MAX_CONTROL_REVERSAL_TICK = 100000

    def __init__(
            self,
            player1_control: PaddleControl,
            player2_control: PaddleControl
    ):
        self._player1, self._player2 = Player.initialize_players()
        self._player1_control = player1_control
        self._player2_control = player2_control
        self._controls_reversed = False

        self._screen = Screen()
        self._ui_painter = Painter(self._screen)

        self._control_reversal_timer = Timer(
            randrange(
                Game.MIN_CONTROL_REVERSAL_TICK,
                Game.MAX_CONTROL_REVERSAL_TICK
            )
        )
        self._object_manager = GameObjectManager(
            self._screen, [self._player1, self._player2]
        )

        player1_control.bind_paddle(self._object_manager.player1_paddle)
        player2_control.bind_paddle(self._object_manager.player2_paddle)

        self._object_manager.init_ball_position()
        self._object_manager.init_ball_motion()

    def loop(self):
        self._object_manager.handle_collisions()
        self._object_manager.move_objects()

        if self._object_manager.is_ball_out_top():
            self._player1.add_points(1)
            self._object_manager.init_ball_position()
            self._object_manager.init_ball_motion(Ball.Direction.TOP)
            self._object_manager.spawn_ball()
        elif self._object_manager.is_ball_out_bottom():
            self._player2.add_points(1)
            self._object_manager.init_ball_position()
            self._object_manager.init_ball_motion(Ball.Direction.BOTTOM)
            self._object_manager.spawn_ball()

        #if self._control_reversal_timer.tick():
        #    self._controls_reversed = not self._controls_reversed
        #    self._player1_control.reverse()
        #    self._player2_control.reverse()
        #    self._control_reversal_timer.reset()

        self._screen.fill()
        self._object_manager.flip_objects()
        self._ui_painter.paint_scoreboard(self.score)
        if self._controls_reversed:
            self._ui_painter.paint_control_reversal_warning()
        if isinstance(self._player1_control, FuzzyControl):
            self._ui_painter.paint_fuzzy_control_state(self._player1_control)
        if isinstance(self._player2_control, FuzzyControl):
            self._ui_painter.paint_fuzzy_control_state(self._player2_control)
        self._screen.flip()

    @property
    def score(self) -> list:
        return [self._player1.score, self._player2.score]

    @property
    def state(self) -> dict:
        ret = dict()
        ret[State.PADDLE1_CENTER_X] = \
            self._object_manager.player1_paddle.center_x
        ret[State.PADDLE1_Y] = \
            self._object_manager.player1_paddle.coordinates.y
        ret[State.PADDLE2_CENTER_X] = \
            self._object_manager.player2_paddle.center_x
        ret[State.PADDLE2_Y] = \
            self._object_manager.player2_paddle.coordinates.y
        ret[State.BALL_X] = self._object_manager.ball.coordinates.x
        ret[State.BALL_Y] = self._object_manager.ball.coordinates.y

        return ret

