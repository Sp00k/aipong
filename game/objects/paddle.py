from enum import Enum
from neuropongoid.game.objects.gameobject import GameObject
from neuropongoid.game.screen.vector import Vector
from neuropongoid.game.screen.screen import Screen
from neuropongoid.game.player import Player


class Paddle(GameObject):
    SCREEN_SIZE_PADDLE_WIDTH_RATIO = 30
    SCREEN_SIZE_PADDLE_HEIGHT_RATIO = 80
    PADDLE_SPEED = 4

    class Placement(Enum):
        BOTTOM = 1
        TOP = 2

    def __init__(self, screen: Screen, player: Player):
        self._player = player
        self._width = screen.w // Paddle.SCREEN_SIZE_PADDLE_WIDTH_RATIO
        self._height = screen.h // Paddle.SCREEN_SIZE_PADDLE_HEIGHT_RATIO
        self._placement = Paddle.Placement.BOTTOM\
            if player.id == 1 else Paddle.Placement.TOP
        self._x = screen.w // 2 - self._width // 2
        self._y = screen.h - self._height\
            if self._placement == Paddle.Placement.BOTTOM else 0
        self._speed_x = 0

    @property
    def coordinates(self) -> Vector:
        return Vector(self._x, self._y)

    @property
    def size(self) -> list:
        return [self._width, self._height]

    @property
    def speed(self) -> Vector:
        return Vector(self._speed_x, 0)

    @property
    def center_x(self):
        return self._x + self._width // 2

    @property
    def collision_y(self):
        return self._y + self._height if self._placement == Paddle.Placement.TOP\
            else self._y

    @property
    def collision_x_left(self):
        return self._x

    @property
    def collision_x_right(self):
        return self._x + self._width

    @property
    def player(self):
        return self._player

    def move(self):
        self._x += self._speed_x
        if self._speed_x > 0:
            self._speed_x -= Paddle.PADDLE_SPEED
        elif self._speed_x < 0:
            self._speed_x += Paddle.PADDLE_SPEED

    def push_left(self):
        self._speed_x -= Paddle.PADDLE_SPEED

    def push_right(self):
        self._speed_x += Paddle.PADDLE_SPEED
