import abc
from neuropongoid.game.screen.vector import Vector


class GameObject(metaclass=abc.ABCMeta):
    @property
    @abc.abstractmethod
    def coordinates(self) -> Vector:
        return NotImplemented

    @property
    @abc.abstractmethod
    def size(self) -> list:
        return NotImplemented

    @property
    @abc.abstractmethod
    def speed(self) -> Vector:
        return NotImplemented

    @abc.abstractmethod
    def move(self):
        return NotImplemented

