from enum import Enum
from neuropongoid.game.objects.gameobject import GameObject
from neuropongoid.game.screen.screen import Screen
from neuropongoid.game.screen.vector import Vector


class Ball(GameObject):
    SCREEN_SIZE_BALL_RADIUS_RATIO = 170
    MAX_SPEED = 22

    class Direction(Enum):
        TOP = 0,
        BOTTOM = 1

    def __init__(self, screen: Screen):
        self._x = None
        self._y = None
        self._radius = screen.size[0] // Ball.SCREEN_SIZE_BALL_RADIUS_RATIO
        self._speed_x = 0
        self._speed_y = 0

    @property
    def coordinates(self) -> Vector:
        return Vector(self._x, self._y)

    @coordinates.setter
    def coordinates(self, value: Vector):
        self._x = value.x
        self._y = value.y

    @property
    def size(self) -> list:
        return [self._radius]

    @property
    def speed(self) -> Vector:
        return Vector(self._speed_x, self._speed_y)

    @speed.setter
    def speed(self, value: Vector):
        self._speed_x = value.x
        self._speed_y = value.y

    @property
    def bottom(self):
        return self._y + self._radius

    @property
    def top(self):
        return self._y - self._radius

    @property
    def left(self):
        return self._x - self._radius

    @property
    def right(self):
        return self._x + self._radius

    @property
    def radius(self):
        return self._radius

    def move(self):
        self._x += self._speed_x
        self._y += self._speed_y

    def invert_x_speed(self):
        self._speed_x *= -1

    def invert_y_speed(self):
        self._speed_y *= -1

    def set_rotation(self, paddle_speed_x: int):
        self._speed_y += 1
        self._speed_x = self._speed_x + paddle_speed_x
        if abs(self._speed_x) > abs(Ball.MAX_SPEED):
            self._speed_x = Ball.MAX_SPEED if self._speed_x > 0 else -Ball.MAX_SPEED
