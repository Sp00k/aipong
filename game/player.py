class Player:
    def __init__(self):
        self._id = None
        self._score = 0

    @property
    def id(self):
        return self._id

    @property
    def score(self):
        return self._score

    def add_points(self, points):
        self._score += points

    @staticmethod
    def initialize_players():
        player1 = Player()
        player1._id = 1
        player2 = Player()
        player2._id = 2
        return [player1, player2]
