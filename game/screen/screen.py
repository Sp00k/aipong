from enum import Enum
import pygame
from neuropongoid.game.screen.vector import Vector


class Screen:
    """PyGame screen adapter"""
    WINDOW_CAPTION = 'PONGOID'
    SCREEN_WIDTH_DEFAULT = 1300
    SCREEN_HEIGHT_DEFAULT = 700

    class Color(Enum):
        BLACK = (0, 0, 0)
        WHITE = (255, 255, 255)

    def __init__(self, width=SCREEN_WIDTH_DEFAULT, height=SCREEN_HEIGHT_DEFAULT):
        self.size = [width, height]
        self._display = pygame.display.set_mode(self.size)
        pygame.display.set_caption(Screen.WINDOW_CAPTION)

    @property
    def w(self):
        return self.size[0]

    @property
    def h(self):
        return self.size[1]

    @property
    def center(self) -> Vector:
        center_x = self.w // 2
        center_y = self.h // 2
        return Vector(center_x, center_y)

    @property
    def surface(self):
        return self._display

    @property
    def border_top(self):
        return 0

    @property
    def border_bottom(self):
        return self.h

    @property
    def border_left(self):
        return 0

    @property
    def border_right(self):
        return self.w

    def fill(self):
        self._display.fill(Screen.Color.BLACK.value)

    @staticmethod
    def flip():
        pygame.display.flip()
