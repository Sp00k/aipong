import pygame
from neuropongoid.game.screen.screen import Screen
from neuropongoid.game.screen.vector import Vector
from neuropongoid.game.timer import Timer
from neuropongoid.game.control.fuzzycontrol import FuzzyControl

class Painter:
    FONT_NAME = 'monospace'
    FONT_SIZE = 20
    BLINK_TICK = 150
    CONTROL_REVERSAL_WARNING_TEXT = 'CONTROLS REVERSED'
    MARGIN_SIZE_RATIO = 30

    def __init__(self, screen: Screen):
        self._screen = screen
        self._font = pygame.font.SysFont(Painter.FONT_NAME, Painter.FONT_SIZE)
        self._blink_timer = Timer(Painter.BLINK_TICK)
        self._blink_switch = True

    def paint_rect(self, vector: Vector, size: list, color=Screen.Color.WHITE.value):
        rect = pygame.Rect(vector.x, vector.y, size[0], size[1])
        pygame.draw.rect(self._screen.surface, color, rect)

    def paint_dot(self, vector: Vector, size: list):
        pos = [vector.x, vector.y]
        pygame.draw.circle(
            self._screen.surface, Screen.Color.WHITE.value, pos, *size
        )

    def _get_vertical_margin_size(self):
        return self._screen.h // Painter.MARGIN_SIZE_RATIO

    def _get_horizontal_margin_size(self):
        return self._screen.w // Painter.MARGIN_SIZE_RATIO

    def _get_scoreboard_position(self):
        return self._get_horizontal_margin_size(),\
               self._get_vertical_margin_size()

    def paint_scoreboard(self, score: list):
        scoreboard = self._font.render(
            f'{score[0]} : {score[1]}',
            1,
            self._screen.Color.WHITE.value
        )
        position = self._get_scoreboard_position()
        self._screen.surface.blit(scoreboard, position)

    def _get_control_reversal_warning_position(self, label):
        return self._screen.w - (label.get_width()
                                 + self._get_horizontal_margin_size()),\
               self._screen.h - (label.get_height()
                                 + self._get_vertical_margin_size())

    def paint_control_reversal_warning(self):
        if self._blink_timer.tick():
            self._blink_switch = not self._blink_switch
            self._blink_timer.reset()

        if self._blink_switch:
            warning = self._font.render(
                f'{Painter.CONTROL_REVERSAL_WARNING_TEXT}',
                1,
                self._screen.Color.WHITE.value
            )
            self._screen.surface.blit(
                warning,
                self._get_control_reversal_warning_position(warning)
            )

    def paint_fuzzy_control_state(self, control: FuzzyControl):
        state = control.state

        r = 255 * state[0] if 1 >= state[0] > 0 else 0
        g = 255 * state[1] if 1 >= state[1] > 0 else 0
        b = 255 * state[2] if 1 >= state[2] > 0 else 0

        if control.paddle_pos.y == 0:
            b_pos = Vector(control.paddle_pos.x, control.paddle_pos.y + 30)
            g_pos = Vector(b_pos.x + 30, control.paddle_pos.y + 30)
            r_pos = Vector(g_pos.x + 30, control.paddle_pos.y + 30)

            self.paint_rect(b_pos, [15, 50 * state[0]], color=(r, 0, 0))
            self.paint_rect(g_pos, [15, 50 * state[1]], color=(0, g, 0))
            self.paint_rect(r_pos, [15, 50 * state[2]], color=(0, 0, b))
        else:
            b_pos = Vector(control.paddle_pos.x, control.paddle_pos.y - 100)
            g_pos = Vector(b_pos.x + 30, control.paddle_pos.y - 100)
            r_pos = Vector(g_pos.x + 30, control.paddle_pos.y - 100)

            self.paint_rect(b_pos, [15, 50 * state[0]], color=(r, 0, 0))
            self.paint_rect(g_pos, [15, 50 * state[1]], color=(0, g, 0))
            self.paint_rect(r_pos, [15, 50 * state[2]], color=(0, 0, b))

