from neuropongoid.game.control.humancontrol import HumanControl


class ControlListener:
    def __init__(self, paddle_controls: list):
        self._paddle_controls = paddle_controls

    def trigger(self, keys_pressed: dict, game_state: dict):
        for control in self._paddle_controls:
            if isinstance(control, HumanControl):
                control.get_input(keys_pressed)
            else:
                control.get_input(game_state)
