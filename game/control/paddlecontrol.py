import abc
from neuropongoid.game.objects.paddle import Paddle


class PaddleControl:
    def __init__(self):
        self._paddle = None

    @abc.abstractmethod
    def get_input(self, event: dict):
        return NotImplemented

    @abc.abstractmethod
    def reverse(self):
        return NotImplemented

    def bind_paddle(self, paddle: Paddle):
        if self._paddle is None:
            self._paddle = paddle

