from dataclasses import dataclass
from neuropongoid.game.control.paddlecontrol import PaddleControl


@dataclass
class KeyboardMap:
    key_left: str
    key_right: str

    def reverse(self):
        self.key_left, self.key_right = self.key_right, self.key_left


class HumanControl(PaddleControl):
    def __init__(self, keyboard_map: KeyboardMap):
        super().__init__()
        self._keyboard_map = keyboard_map

    def get_input(self, keys_pressed: dict):
        if keys_pressed[self._keyboard_map.key_left]:
            self._paddle.push_left()
        if keys_pressed[self._keyboard_map.key_right]:
            self._paddle.push_right()

    def reverse(self):
        self._keyboard_map.reverse()




