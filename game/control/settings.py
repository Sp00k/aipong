import pygame
from neuropongoid.game.control.humancontrol import KeyboardMap


KEYBOARD_CONTROL_1_SETTINGS = KeyboardMap(
    key_left=pygame.K_LEFT,
    key_right=pygame.K_RIGHT
)

KEYBOARD_CONTROL_2_SETTINGS = KeyboardMap(
    key_left=pygame.K_a,
    key_right=pygame.K_d
)
