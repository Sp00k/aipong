from neuropongoid.game.control.fuzzycontrol import FuzzyControl, State


class DummyControl(FuzzyControl):
    def get_input(self, positions: dict):
        player_id = self._paddle.player.id

        p_cx = positions[State.PADDLE1_CENTER_X] if player_id == 1 \
            else positions[State.PADDLE2_CENTER_X]
        b_x = positions[State.BALL_X]

        if b_x > p_cx:
            self._paddle.push_right()
        elif b_x < p_cx:
            self._paddle.push_left()
        else:
            return
