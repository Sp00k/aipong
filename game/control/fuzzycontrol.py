import numpy as np
from neuropongoid.game.control.paddlecontrol import PaddleControl
from neuropongoid.game.objects.paddle import Paddle
from neuropongoid.game.screen.screen import Screen
from neuropongoid.ai.environment import State


def dev_l(screen_w: int, ball_x: int):
    ret = (screen_w / 2 - ball_x) / (screen_w / 2)
    return ret if ret > 0 else 0


def dev_r(screen_w: int, ball_x: int):
    ret = (ball_x - screen_w / 2) / (screen_w / 2)
    return ret if ret > 0 else 0


def dev(screen_w: int, ball_x: int):
    l = dev_l(screen_w, ball_x)
    r = dev_l(screen_w, ball_x)
    return l if l > 0 else r


def dist_l(screen_w: int, ball_x: int, paddle_center: int):
    ret = (paddle_center - ball_x) / screen_w
    return ret if ret > 0 else 0


def dist_r(screen_w: int, ball_x: int, paddle_center: int):
    ret = (ball_x - paddle_center) / screen_w
    return ret if ret > 0 else 0


def ball_y_dist(screen_h: int, ball_y: int, paddle_y: int):
    return abs(paddle_y - ball_y) / screen_h


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


class FuzzyControl(PaddleControl):
    def __init__(self):
        super().__init__()
        self._state_left = 0
        self._state_right = 0
        self._state_none = 0

    def get_input(self, positions: dict):
        player_id = self._paddle.player.id

        p_cx = positions[State.PADDLE1_CENTER_X] if player_id == 1\
            else positions[State.PADDLE2_CENTER_X]
        p_y = positions[State.PADDLE1_Y] if player_id == 1\
            else positions[State.PADDLE2_Y]
        b_x = positions[State.BALL_X]
        b_y = positions[State.BALL_Y]
        s_w = Screen.SCREEN_WIDTH_DEFAULT
        s_h = Screen.SCREEN_HEIGHT_DEFAULT

        none = 1 / (1 + np.exp(-20*(ball_y_dist(s_h, b_y, p_y)-0.4)))
        left = dist_l(s_w, b_x, p_cx)
        right = dist_r(s_w, b_x, p_cx)

        if left + right == 0:
            return

        left = (1-none/2) * (left / (left + right))
        right = (1-none/2) * (right / (left + right))
        none /= 2

        self._state_left = left
        self._state_right = right
        self._state_none = none

        choice = np.random.choice([0, 1, 2], p=[left, right, none])
        if choice == 0:
            self._paddle.push_left()
        elif choice == 1:
            self._paddle.push_right()
        else:
            if p_cx - s_w / 2 > 0:
                self._paddle.push_left()
            elif p_cx - s_w / 2 < 0:
                self._paddle.push_right()

    def reverse(self):
        pass

    def bind_paddle(self, paddle: Paddle):
        super().bind_paddle(paddle)

    @property
    def state(self):
        return (
            self._state_left,
            self._state_none,
            self._state_right
        )

    @property
    def paddle_pos(self):
        return self._paddle.coordinates

