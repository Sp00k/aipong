import random
from neuropongoid.game.objects.paddle import Paddle
from neuropongoid.game.objects.ball import Ball
from neuropongoid.game.screen.screen import Screen
from neuropongoid.game.screen.painter import Painter
from neuropongoid.game.screen.vector import Vector
from neuropongoid.game.timer import Timer


class GameObjectManager:
    BALL_SPAWN_TICKS = 700
    BALL_MOVE_TICKS = 5
    BALL_INITIAL_SPEED = 7

    def __init__(self, screen: Screen, players: list):
        self._ball_spawn_timer = Timer(GameObjectManager.BALL_SPAWN_TICKS)
        self._ball_move_timer = Timer(GameObjectManager.BALL_MOVE_TICKS)

        self._painter = Painter(screen)
        self._screen = screen

        self._player1_paddle = Paddle(screen, players[0])
        self._player2_paddle = Paddle(screen, players[1])
        self._ball = Ball(screen)

    @property
    def player1_paddle(self):
        return self._player1_paddle

    @property
    def player2_paddle(self):
        return self._player2_paddle

    @property
    def ball(self):
        return self._ball

    def move_objects(self):
        self._player1_paddle.move()
        self._player2_paddle.move()

        if self._ball_spawn_timer.tick() and self._ball_move_timer.tick():
            self._ball.move()
            self._ball_move_timer.reset()

    def flip_objects(self):
        self._painter.paint_rect(
            self._player1_paddle.coordinates,
            self._player1_paddle.size
        )

        self._painter.paint_rect(
            self._player2_paddle.coordinates,
            self._player2_paddle.size
        )

        self._painter.paint_dot(
            self._ball.coordinates,
            self._ball.size
        )

    def init_ball_position(self):
        self._ball.coordinates = self._screen.center

    def init_ball_motion(self, direction: Ball.Direction=None):
        abs_ball_speed = GameObjectManager.BALL_INITIAL_SPEED

        y_speed = random.choice(
            [

                abs_ball_speed,
                -abs_ball_speed,

            ])

        x_speed = random.choice(
            [
                abs_ball_speed,
                -abs_ball_speed,
            ]
        )

        self._ball.speed = Vector(x_speed, y_speed)

    def _check_player1_paddle_collision(self):
        y_collision = self._ball.coordinates.y + self.ball.radius >= self._player1_paddle.collision_y - self._ball.speed.y
        x_collision = self._player1_paddle.collision_x_left <= self._ball.right - self.ball.speed.x \
            and self._ball.left - self.ball.speed.x <= self._player1_paddle.collision_x_right

        return y_collision and x_collision

    def _check_player2_paddle_collision(self):
        y_collision = self._ball.coordinates.y - self.ball.radius <= self._player2_paddle.collision_y - self.ball.speed.y
        x_collision = self._player2_paddle.collision_x_left <= self._ball.right - self.ball.speed.x\
            and self._ball.left - self.ball.speed.x <= self._player2_paddle.collision_x_right

        return y_collision and x_collision

    def _check_ball_wall_collision(self):
        return self._ball.coordinates.x <= self._screen.border_left \
            or self._ball.coordinates.x >= self._screen.border_right

    def _check_paddle_left_wall_collision(self, paddle: Paddle):
        return paddle.collision_x_left <= self._screen.border_left

    def _check_paddle_right_wall_collision(self, paddle: Paddle):
        return paddle.collision_x_right >= self._screen.border_right

    def handle_collisions(self):
        if self._check_player1_paddle_collision():
            self._ball.invert_y_speed()
            self._ball.set_rotation(self._player1_paddle.speed.x // 3)

        if self._check_player2_paddle_collision():
            self._ball.invert_y_speed()
            self._ball.set_rotation(self._player2_paddle.speed.x // 3)

        if self._check_ball_wall_collision():
            self._ball.invert_x_speed()

        if self._check_paddle_left_wall_collision(self._player1_paddle):
            self.player1_paddle.push_right()

        if self._check_paddle_right_wall_collision(self._player1_paddle):
            self.player1_paddle.push_left()

        if self._check_paddle_left_wall_collision(self.player2_paddle):
            self.player2_paddle.push_right()

        if self._check_paddle_right_wall_collision(self.player2_paddle):
            self.player2_paddle.push_left()

    def is_ball_out_top(self):
        return self._ball.coordinates.y - self._ball.radius < self._screen.border_top

    def is_ball_out_bottom(self):
        return self._ball.coordinates.y + self._ball.radius > self._screen.border_bottom

    def spawn_ball(self):
        self._ball_spawn_timer.reset()
